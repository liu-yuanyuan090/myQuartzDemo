package com.lyy.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("myjob")
public class MyJob {
    @TableId("job_id")
    private Integer jobId;
    private String jobName;
    private String jobGroup;
    private String cronExpression;
    /** 任务状态（0正常 1暂停） */
    private String status;
}
