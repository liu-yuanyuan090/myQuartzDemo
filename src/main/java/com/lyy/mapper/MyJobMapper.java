package com.lyy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyy.domain.MyJob;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MyJobMapper extends BaseMapper<MyJob> {

}
