package com.lyy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lyy.domain.MyJob;
import com.lyy.mapper.MyJobMapper;
import com.lyy.quartz.JobExecution1;
import com.lyy.quartz.listeners.MyListener;
import com.lyy.quartz.listeners.MySchedulerListener;
import com.lyy.quartz.listeners.MyTriggerListener;
import com.lyy.service.MyJobService;
import org.quartz.*;
import org.quartz.impl.matchers.EverythingMatcher;
import org.quartz.impl.matchers.KeyMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.time.DateFormatUtils;
import java.util.Date;

@Service
public class MyJobServiceImpl extends ServiceImpl<MyJobMapper, MyJob> implements MyJobService {

    @Autowired
    MyJobMapper myJobMapper;
    @Autowired
    SchedulerFactoryBean schedulerFactoryBean;
    @Autowired
    Scheduler scheduler;


    @Override
    public void add(MyJob myJob) {
        //1.创建job
        //调度器
        try {
//        Scheduler scheduler = schedulerFactoryBean.getScheduler();
            JobDetail jobDetail = JobBuilder.newJob(JobExecution1.class)
                    .withIdentity(myJob.getJobName(),myJob.getJobGroup())
                    .storeDurably()
                    .usingJobData("addMap","addMap")
                    .build();
            //TODO DataMap 这个地方怎么完善
//            jobDetail.getJobDataMap().put("addMap","addMap");
        CronScheduleBuilder cronSchedule = CronScheduleBuilder.cronSchedule(myJob.getCronExpression());

        CronTrigger trigger = TriggerBuilder.newTrigger()
                .withIdentity(myJob.getJobName()+"_trigger_name",myJob.getJobGroup()+"_trigger_group")
                .withSchedule(cronSchedule)
                .usingJobData("trigger_key","trigger_value")
                .build();
            // 判断是否存在
            if(scheduler.checkExists(JobKey.jobKey(myJob.getJobName(),myJob.getJobGroup())))
            {
                //先移除，再创建，防止数据不一致
                scheduler.deleteJob(JobKey.jobKey(myJob.getJobName(),myJob.getJobGroup()));
            }

            // 执行调度任务
            scheduler.scheduleJob(jobDetail, trigger);

            // 6. 创建并注册一个全局的Job Listener
            scheduler.getListenerManager().addJobListener(new MyListener(), EverythingMatcher.allJobs());

            // 7. 创建一个局部的Job Listener 所谓的局部就是指定的job
            //scheduler.getListenerManager().addJobListener(new MyListener(), KeyMatcher.keyEquals(JobKey.jobKey("job1","group1")));

            // 8. 创建并注册一个全局的Trigger Listener
            scheduler.getListenerManager().addTriggerListener(new MyTriggerListener("simpleTrigger"), EverythingMatcher.allTriggers());

            // 9. 创建并注册一个局部的Trigger Listener
            //scheduler.getListenerManager().addTriggerListener(new MyTriggerListener("simpleTrigger"), KeyMatcher.keyEquals(TriggerKey.triggerKey("trigger1", "group1")));

            //10.注册 Scheduler 全局触发器, 没有局部触发器
            //scheduler.getListenerManager().addSchedulerListener(new MySchedulerListener());

            //Job Listener与Trigger Listener用的多，Scheduler Listener用的少

            //addJob的含义
            //增 scheduler.addJob(jobDetail,false);false表示不替换,ture表示替换,既更新;改 sched.addJob(jobDetail,true); 用来更新job

            //另外一般使用 比较方便sched.scheduleJob(jobDetail, trigger);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }


        //2.插入数据库
        myJobMapper.insert(myJob);

    }

    @Override
    public void run(MyJob myJob) {
        try {
//            Scheduler scheduler = schedulerFactoryBean.getScheduler();
            if(scheduler.checkExists(JobKey.jobKey(myJob.getJobName(),myJob.getJobGroup()))) {
                // 参数
                JobDataMap dataMap = new JobDataMap();
                dataMap.put("runMap","runMap");
                scheduler.triggerJob(JobKey.jobKey(myJob.getJobName(), myJob.getJobGroup()),dataMap);
            }
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateJob(MyJob myJob) {
        try {
            //更新Job
//            Scheduler scheduler = schedulerFactoryBean.getScheduler();
            JobDetail jobDetail = JobBuilder.newJob(JobExecution1.class)
                    .withIdentity(myJob.getJobName(),myJob.getJobGroup())
                    .storeDurably()
                    .withDescription(DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"))
                    .build();
            CronScheduleBuilder cronSchedule = CronScheduleBuilder
                    .cronSchedule(myJob.getCronExpression())
                    .withMisfireHandlingInstructionDoNothing();
            CronTrigger trigger = TriggerBuilder.newTrigger()
                    .withDescription(DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"))
                    .withIdentity(myJob.getJobName()+"_trigger_name",myJob.getJobGroup()+"_trigger_group")
                    .withSchedule(cronSchedule).build();
            if(scheduler.checkExists(JobKey.jobKey(myJob.getJobName(),myJob.getJobGroup())))
            {
                //暂停任务
                //scheduler.pauseJob(JobKey.jobKey(myJob.getJobName(),myJob.getJobGroup()));
                //重启
                //scheduler.resumeJob(JobKey.jobKey(myJob.getJobName(),myJob.getJobGroup()));
                //先移除，再创建，防止数据不一致
                scheduler.deleteJob(JobKey.jobKey(myJob.getJobName(),myJob.getJobGroup()));
            }
            scheduler.scheduleJob(jobDetail,trigger);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
        //更新数据库
        baseMapper.updateById(myJob);

    }

    @Override
    public void deleteJob(MyJob myJob) {
        try {
            //更新Job
//            Scheduler scheduler = schedulerFactoryBean.getScheduler();
            scheduler.deleteJob(JobKey.jobKey(myJob.getJobName(),myJob.getJobGroup()));
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
        //更新数据库
        baseMapper.deleteById(myJob.getJobId());
    }


}
