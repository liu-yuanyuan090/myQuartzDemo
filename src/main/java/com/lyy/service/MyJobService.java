package com.lyy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyy.domain.MyJob;



public interface MyJobService extends IService<MyJob> {

    void add(MyJob myJob);

    void run(MyJob myJob);

    void updateJob(MyJob myJob);

    void deleteJob(MyJob myJob);
}
