package com.lyy.quartz;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

/**
 * SpringBoot项目常用QuartzJobBean
 * 加入容器管理，方便注入其他bean，编写业务逻辑
 */
@Component
public class JobExecution1 extends QuartzJobBean {

//    @Autowired
//    YourService yourService;


    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
        // 获取jobDetail存储的参数
//        long taskId = jobDataMap.getLongValue("taskId");

        //任务执行逻辑
        //yourService.startTask(taskId);


        System.out.println("执行了任务");
    }
}
