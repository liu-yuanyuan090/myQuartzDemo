package com.lyy.quartz.listeners;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;

public class MyListener implements JobListener {
    @Override
    // 用于获取该JobListener的名称。
    public String getName() {
        String simpleName = this.getClass().getSimpleName();
        System.out.println("监听器的名称是："+simpleName);
        return simpleName;
    }

    @Override
    // Scheduler在JobDetail将要被执行时调用这个方法.
    public void jobToBeExecuted(JobExecutionContext jobExecutionContext) {

        String name = jobExecutionContext.getJobDetail().getKey().getName();
        System.out.println("Scheduler在jobDetail将要被执行时调用的方法"+name);

    }

    @Override

    // Scheduler在JobDetail即将被执行，但又被TriggerListerner否决时会调用该方法
    public void jobExecutionVetoed(JobExecutionContext jobExecutionContext) {

        String name = jobExecutionContext.getJobDetail().getKey().getName();
        System.out.println("job将要被执行，但是又被trigger否决时执行的方法："+name);

    }

    @Override

    // Scheduler在JobDetail被执行之后调用这个方法
    public void jobWasExecuted(JobExecutionContext jobExecutionContext, JobExecutionException e) {

        String name = jobExecutionContext.getJobDetail().getKey().getName();
        System.out.println("job被执行后调用的方法："+name);

    }
}
