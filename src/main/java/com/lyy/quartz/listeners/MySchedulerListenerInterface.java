package com.lyy.quartz.listeners;

import org.quartz.SchedulerException;
import org.springframework.scheduling.Trigger;

/**
 * 接口源码，根据需要实现接口
 */
public interface MySchedulerListenerInterface {

    // 用于部署JobDetail时调用.
    public void jobScheduled(Trigger trigger);

    // 用于卸载JobDetail时调用
    public void jobUnscheduled(String triggerName, String triggerGroup);

    /* 当一个 Trigger 来到了再也不会触发的状态时调用这个方法。除非这个 Job 已设置
    成了持久性，否则它就会从 Scheduler 中移除。*/
    public void triggerFinalized(Trigger trigger);

    /* Scheduler 调用这个方法是发生在一个 Trigger 或 Trigger 组被暂停时。
    假如是 Trigger 组的话，triggerName 参数将为 null。*/
    public void triggersPaused(String triggerName, String triggerGroup);

    /*Scheduler 调用这个方法是发生成一个 Trigger 或 Trigger 组从暂停中恢复时。
    假如是 Trigger 组的话，假如是 Trigger 组的话，triggerName 参数将为 null。参数将为 null。*/
    public void triggersResumed(String triggerName, String triggerGroup);

    // 当一个或一组 JobDetail 暂停时调用这个方法。
    public void jobsPaused(String jobName, String jobGroup);

    // 当一个或一组 Job 从暂停上恢复时调用这个方法。假如是一个 Job 组，jobName 参数将为 null。
    public void jobsResumed(String jobName, String jobGroup);

    // 在 Scheduler 的正常运行期间产生一个严重错误时调用这个方法。
    public void schedulerError(String msg, SchedulerException cause);

    // 当Scheduler 开启时，调用该方法.
    public void schedulerStarted();

    // 当Scheduler处于StandBy模式时，调用该方法.
    public void schedulerInStandbyMode();

    // 当Scheduler停止时，调用该方法.
    public void schedulerShutdown();

    // 当Scheduler中的数据被清除时，调用该方法。
    public void schedulingDataCleared();
}
