package com.lyy.quartz.listeners;

import org.quartz.*;

public class MySchedulerListener implements SchedulerListener {
    @Override
    public void jobScheduled(Trigger trigger) {
        System.out.println("用于部署JobDetail时调用");
    }

    @Override
    public void jobUnscheduled(TriggerKey triggerKey) {
        System.out.println("用于卸载JobDetail时调用");
    }

    @Override
    public void triggerFinalized(Trigger trigger) {
        System.out.println("当一个 Trigger 来到了再也不会触发的状态时调用这个方法。<br/>除非这个 Job 已设置成了持久性，否则它就会从 Scheduler 中移除");
    }

    @Override
    public void triggerPaused(TriggerKey triggerKey) {
        System.out.println("Scheduler 调用这个方法是发生在一个 Trigger被暂停时");
    }

    @Override
    public void triggersPaused(String s) {
        System.out.println("Scheduler 调用这个方法是发生在一个 Trigger 组被暂停时。<br/>假如是 Trigger 组的话，triggerName 参数将为 null");
    }

    @Override
    public void triggerResumed(TriggerKey triggerKey) {
        System.out.println("Scheduler 调用这个方法是发生成一个 Trigger从暂停中恢复时");
    }

    @Override
    public void triggersResumed(String s) {
        System.out.println("Scheduler 调用这个方法是发生成一个 Trigger 组从暂停中恢复时。假如是 Trigger 组的话，triggerName 参数将为 null。");
    }

    @Override
    public void jobAdded(JobDetail jobDetail) {
        System.out.println("添加job时调用");
    }

    @Override
    public void jobDeleted(JobKey jobKey) {
        System.out.println("删除Job时调用");
    }

    @Override
    public void jobPaused(JobKey jobKey) {
        System.out.println("挂起Job时调用");
    }

    @Override
    public void jobsPaused(String s) {
        System.out.println("挂起jobs组时调用，假如是组的话，参数将为null");
    }

    @Override
    public void jobResumed(JobKey jobKey) {
        System.out.println("恢复Job时调用");
    }

    @Override
    public void jobsResumed(String s) {
        System.out.println("恢复Job组时调用，假如是组的话，参数将为null");
    }

    @Override
    public void schedulerError(String s, SchedulerException e) {
        System.out.println("在 Scheduler 的正常运行期间产生一个严重错误时调用这个方法");
    }

    @Override
    public void schedulerInStandbyMode() {
        System.out.println("当Scheduler处于StandBy模式时，调用该方法");
    }

    @Override
    public void schedulerStarted() {
        System.out.println("当Scheduler 开启时，调用该方法");
    }

    @Override
    public void  schedulerStarting() {
        System.out.println("当Scheduler 开启中，调用该方法");
    }

    @Override
    public void schedulerShutdown() {
        System.out.println("当Scheduler停止时，调用该方法");
    }

    @Override
    public void schedulerShuttingdown() {
        System.out.println("当Scheduler停止中，调用该方法");
    }

    @Override
    public void schedulingDataCleared() {
        System.out.println("当Scheduler中的数据被清除时，调用该方法");
    }
}

