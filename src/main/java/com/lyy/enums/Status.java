package com.lyy.enums;

public enum Status {
    /**
     * 正常
     */
    NORMAL("0"),
    /**
     * 暂停
     */
    PAUSE("1");

    private String value;

    private Status(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return value;
    }
    //使用
    //job.setStatus(ScheduleConstants.Status.PAUSE.getValue());
}
