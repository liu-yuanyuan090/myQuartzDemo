package com.lyy.controller;

import com.lyy.domain.MyJob;
import com.lyy.service.MyJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MyJobController {

    @Autowired
    MyJobService myJobService;
    /**
     * 查询定时任务列表
     */
    @GetMapping("/list")
    //TODO 需要传参数吗？？？？
    public List<MyJob> list()
    {
        return myJobService.list();
    }
    /**
     * 获取定时任务详细信息
     */
    @GetMapping("/getInfo")
    public MyJob getInfo(@RequestParam("jobId") Integer jobId)
    {
        return  myJobService.getById(jobId);
    }

    /**
     * 新增定时任务
     */
    @PostMapping("/add")
    public String add(@RequestBody MyJob myJob)
    {
        myJobService.add(myJob);
        return "添加成功";
    }
    /**
     * 修改定时任务
     */
    @PostMapping("/update")
    public String update(@RequestBody MyJob myJob)
    {
        myJobService.updateJob(myJob);
        return "修改成功";
    }
    /**
     * 删除定时任务
     */
    @PostMapping("/delete")
    public String delete(@RequestBody MyJob myJob)
    {
        myJobService.deleteJob(myJob);
        return "删除成功";
    }
    /**
     * 定时任务立即执行一次
     */
    @PostMapping("/run")
    public String run(@RequestBody MyJob myJob)
    {
        myJobService.run(myJob);
        return "立即执行一次了";
    }


}
