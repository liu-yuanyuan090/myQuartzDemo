package com.lyy.constants;

/**
 * Misfire：当一个作业在配置的规定时间没有运行（比如线程池里面没有可用的线程、作业被暂停等）
 * 并且作业配置的应该运行时刻为A,当前时间为B，如果B与A的时间间隔超过misfireThreshold配置的值
 *（默认为60秒）则作业会被调度程序认为Misfire。
 */
public class MisFireConstants {
    /** 默认 */
    public static final String MISFIRE_DEFAULT = "0";

    /** 立即触发执行 */
    public static final String MISFIRE_IGNORE_MISFIRES = "1";

    /** 触发一次执行 */
    public static final String MISFIRE_FIRE_AND_PROCEED = "2";

    /** 不触发立即执行
     * 对于错失运行的不做任何处理
     */
    public static final String MISFIRE_DO_NOTHING = "3";
}
